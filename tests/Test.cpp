// Test.cpp : Defines the entry point for the console application.
//

#include <iostream>

#include "../src/TaskSystem.hpp"

#include <thread>
#include <chrono>
#include <functional>

using namespace std;


int CountOddsSequential( int* n, int size )
{
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

	int value = 0;
	for( int i = 0; i < size; i++ )
	{
		if( n[i] % 2 )
			value++;
	}

	std::chrono::milliseconds duration = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - start );

	cout << "Sequential Execution Time: " << duration.count() << endl;

	return value;
}

int CountOddsParallel( int* n, int size )
{
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

	std::atomic<int> value = 0;

	long long time = 0;

	for( int i = 0; i < size; i++ )
	{
		int data = n[i];
		std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
		MalpenTask::TaskSystem::Add( [data, &value] ()
		{
			if( data % 2 )
				value++;
		} );
		std::chrono::milliseconds duration = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - start );
		
		time += duration.count();
	}

	cout << "tempo aggiunta task Execution Time: " << time << endl;

	MalpenTask::TaskSystem::RunAll();

	MalpenTask::TaskSystem::WaitAll();

	std::chrono::milliseconds duration = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - start );

	cout << "Parallel Execution Time: " << duration.count() << endl;

	return value;
}

void basicTest()
{
	MalpenTask::TaskSystem::Init( 8 );

	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "A"; } );
	MalpenTask::TaskId b = MalpenTask::TaskSystem::Add( 
		[] () 
		{ 
			std::this_thread::sleep_for (std::chrono::seconds(5));

			cout << "B"; 
		} 
	);
	MalpenTask::TaskId c = MalpenTask::TaskSystem::Add( [] () { cout << "C"; } );
	MalpenTask::TaskId d = MalpenTask::TaskSystem::Add( [] () { cout << "D"; } );

	MalpenTask::TaskSystem::AddChild( c, b );
	MalpenTask::TaskSystem::AddChild( d, c );

	MalpenTask::TaskSystem::Run( d );
	MalpenTask::TaskSystem::Run( c );
	MalpenTask::TaskSystem::Run( b );

	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "E"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "F"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "G"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "H"; } );
	MalpenTask::TaskId i = MalpenTask::TaskSystem::Add( [] () 
		{ 
			std::this_thread::sleep_for (std::chrono::seconds(7));

			cout << "I"; 
		} 
	);

	MalpenTask::TaskSystem::Run( i );

	MalpenTask::TaskSystem::Wait( i );

	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "L"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "M"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "N"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "O"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "P"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "Q"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "R"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "S"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "T"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "U"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "V"; } );
	MalpenTask::TaskSystem::AddAndRun( [] () { cout << "Z"; } );

	MalpenTask::TaskSystem::Stop();
}

void CountOddsTest()
{
	MalpenTask::TaskSystem::Init( 8 );

	int ArraySize = 0;

	cout << "Insert array Size " << endl;

	cin >> ArraySize;

	int* arrayData = new int[ArraySize];

	cout << "Array generation in progress" << endl;

	srand( time( nullptr ) );

	int numOdds = 0;
	for( int i = 0; i < ArraySize; i++ )
	{
		arrayData[i] = rand();
		if( arrayData[i] % 2 )
			numOdds++;
	}

	cout << "Array generation completed: odds number = " << numOdds << endl;

	int oddsNumSeq = CountOddsSequential( arrayData, ArraySize );
	int oddsNumParal = CountOddsParallel( arrayData, ArraySize );

	cout << "Sequential value " << oddsNumSeq << " Parallel value " << oddsNumParal << endl;

	MalpenTask::TaskSystem::Stop();
}

int main( int argc, char** argv )
{
	cout << "MalpenTask system test" << endl;

	char continueChar;

	do
	{
		CountOddsTest();

		cout << endl << "Retry? (n for exit)" << endl;

		cin >> continueChar;

	} while( continueChar != 'n' );

	return 0;
}


