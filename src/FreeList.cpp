#include "FreeList.hpp"
#include <iostream>
#include <chrono>

using namespace std;

long long value = 0;

namespace MalpenTask
{
	FreeList::FreeList( void* start, void* end, size_t elementSize )
	  : mNext( nullptr )
	{
		union
		{
			void* asVoid;
			char* asChar;
			Self* asSelf;
		};
 
		asVoid = start;
		mNext = asSelf;
 
		const size_t numElements = ( static_cast<char*>( end ) - asChar ) / elementSize;
 
		asChar += elementSize;
 
		// initialize the free list - make every m_next at each element point to the next element in the list
		Self* runner = mNext;
		for( size_t i = 1; i < numElements; ++i )
		{
			runner->mNext = asSelf;
			runner = runner->mNext;
 
			asChar += elementSize;	
		}
 
		runner->mNext = nullptr;
	}
 
	void* FreeList::Obtain( void )
	{
		std::unique_lock<std::mutex> lock( mut );
		if( mNext == nullptr )
			noSpaceCond.wait( lock );

		// obtain one element from the head of the free list
		Self* head = mNext;
		mNext = head->mNext;

		return head;
	}
 
	void FreeList::Return( void* ptr )
	{
		std::unique_lock<std::mutex> lock( mut );

		// put the returned element at the head of the free list
		Self* head = static_cast<Self*>( ptr );
		head->mNext = mNext;
		mNext = head;

		noSpaceCond.notify_all();
	}
}