#ifndef FREELIST_HPP
#define FREELIST_HPP

#include <condition_variable>
#include <mutex>

namespace MalpenTask
{
	class FreeList
	{
		typedef FreeList Self;
 
		public:
			FreeList( void* start, void* end, size_t elementSize );
 
			void* Obtain( void );
 
			void Return( void* ptr );
 
		private:
			Self* mNext;

			std::condition_variable noSpaceCond;
			std::mutex mut;
	};
}

#endif