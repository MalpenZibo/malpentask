#ifndef TASK_HPP
#define TASK_HPP

#include <functional>
#include <atomic>

namespace MalpenTask
{
	struct Task
	{
		bool IsRunning;
		unsigned int Generation;
		std::atomic<unsigned int> OpenTasks;
		unsigned int Parent;
		std::function<void ()> Function;
	};
}

#endif