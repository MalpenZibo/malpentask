#ifndef TASKID_HPP
#define TASKID_HPP

namespace MalpenTask
{
	struct TaskId
	{
		unsigned int Offset;
		unsigned int Generation;
 
		TaskId( unsigned int offset, unsigned int generation )
			: Offset( offset )
			, Generation( generation )
		{
		}
	};
}

#endif