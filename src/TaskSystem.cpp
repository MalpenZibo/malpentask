#include "TaskSystem.hpp"

#include <utility>
#include <iterator>

namespace MalpenTask
{
	bool TaskSystem::isInitialized = false;
	bool TaskSystem::isRunning = false;

	std::queue<Task*> TaskSystem::tasks;

	std::vector<std::thread> TaskSystem::threads;
	std::condition_variable TaskSystem::noJobCond;
	std::mutex TaskSystem::mut;

	
	const unsigned int TaskSystem::TASK_POOL_SIZE;
	char TaskSystem::taskPoolMemory[];
	std::unique_ptr<FreeList> TaskSystem::mTaskPool;

	void TaskSystem::Init( int threadNumber )
	{
		isRunning = true;

		mTaskPool = std::unique_ptr<FreeList>( new FreeList( taskPoolMemory, taskPoolMemory + TASK_POOL_SIZE, sizeof(Task) ) );

		std::unique_lock<std::mutex> lock( mut );
	
		for( int i = 0; i < threadNumber; i++ )
		{
			threads.push_back( std::thread( Scheduler ) );
		}

		isInitialized = true;
	}

	void TaskSystem::Stop()
	{
		WaitAll();

		isRunning = false;

		noJobCond.notify_all();

		for( std::vector<std::thread>::iterator it = threads.begin(); it != threads.end(); ++it ) 
		{
			it->join();
		}

		threads.clear();
	}

	void TaskSystem::ClearTasks()
	{
		std::unique_lock<std::mutex> lock( mut );

		while( tasks.size() > 0 )
		{	
			tasks.pop();
		}
	}

	void TaskSystem::Scheduler()
	{
		while( isRunning )
		{
			Task* task = WaitUntilTaskIsAvailable();
			if( task != nullptr )
				WorkOnTask( task );
		}
	}

	Task* TaskSystem::WaitUntilTaskIsAvailable()
	{
		Task* task = nullptr;	

		while( isRunning )
		{
			std::unique_lock<std::mutex> lock( mut );

			if( tasks.size() > 0 )
			{
				task = tasks.front();
				tasks.pop();

				return task;
			}
			else
			{
				if( isRunning )
					noJobCond.wait( lock );
			}				
		}		
	}

	void TaskSystem::HelpWithWork()
	{
		Task* task = nullptr;

		{
			std::unique_lock<std::mutex> lock( mut );

			if( tasks.size() > 0 )
			{
				task = tasks.front();
				tasks.pop();
			}
		}
		
		if( task != nullptr )
			WorkOnTask( task );
		else
			std::this_thread::yield();
	}

	void TaskSystem::WorkOnTask( Task* task )
	{
		while( !CanExecuteTask( task ) )
		{
			// the task cannot be executed at this time, work on another item
			HelpWithWork();
		}

		task->Function();

		FinishTask( task );
	}

	void TaskSystem::FinishTask( Task* task )
	{
		const unsigned int lOpenTasks = --task->OpenTasks;
				
		if( lOpenTasks == 0 )
		{
			if( task->Parent != TaskValue::NO_PARENT )
			{
				Task* parent = GetTask( task->Parent );
				FinishTask( parent );
			}

			mTaskPool->Return( task );
		}
	}

	Task* TaskSystem::GetScheduledTask()
	{
		std::unique_lock<std::mutex> lock( mut );

		Task* task = nullptr;		

		if( tasks.size() > 0 )
		{	
			task = tasks.front();
			tasks.pop();
		}

		return task;
	}

	TaskId TaskSystem::Add( const std::function<void ()>& function )
	{
		Task* task = (Task*)mTaskPool->Obtain();
		
		task->Function = function;
		task->Generation++;
		task->OpenTasks = 1;	
		task->IsRunning = false;
		task->Parent = TaskValue::NO_PARENT;

		return GetTaskId( task );
	}

	void TaskSystem::AddChild( const TaskId& parentId, const TaskId& childId )
	{
		Task* parent = GetTask( parentId );
		Task* child = GetTask( childId );

		if( !parent->IsRunning && !child->IsRunning )
		{
			if( child->Parent != TaskValue::NO_PARENT )
			{
				Task* oldParent = GetTask( child->Parent );
				oldParent->OpenTasks--;
			}

			child->Parent = parentId.Offset;
			parent->OpenTasks++;
		}
	}

	void TaskSystem::Run( const TaskId& taskId )
	{
		Task* task = GetTask( taskId );

		task->IsRunning = true;

		{
			std::unique_lock<std::mutex> lock( mut );

			tasks.push( task );
		}

		noJobCond.notify_all();
	}

	void TaskSystem::RunAll()
	{
		std::unique_lock<std::mutex> lock( mut );

		std::iterator<std::queue<Task*>> it( tasks );
		while( tasks.size() > 0 )
		{	
			tasks.pop();
		}
		for
	}

	TaskId TaskSystem::AddAndRun( const std::function<void ()>& function )
	{
		TaskId ret = Add( function );
		Run( ret );

		return ret;
	}

	void TaskSystem::Wait( const TaskId& taskId )
	{
		Task* target = GetTask( taskId );

		if( target->IsRunning )
		{
			// wait until the task and all its children have completed
			while( !IsTaskFinished( taskId ) )
			{
				// help with working while the task isn't finished
				HelpWithWork();
			}
		}
	}

	void TaskSystem::WaitAll()
	{
		while( true )
		{
			Task* task = nullptr;

			{
				std::unique_lock<std::mutex> lock( mut );

				if( tasks.size() > 0 )
				{	
					task = tasks.front();
				}
				else
					return;
			}

			Wait( GetTaskId( task ) );
		}
	}
}