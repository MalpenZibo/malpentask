#ifndef TASK_SYSTEM_HPP
#define TASK_SYSTEM_HPP

#include <queue>
#include <vector>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <memory>

#include "Task.hpp"
#include "TaskId.hpp"
#include "FreeList.hpp"

namespace MalpenTask
{
	enum TaskValue
	{
		NO_PARENT = -1
	};

	class TaskSystem
	{
		public:

			static void Init( int threadNumber );

			static void Stop();

			static TaskId Add( const std::function<void ()>& function );
			static void AddChild( const TaskId& parentId, const TaskId& childId );
			static TaskId AddAndRun( const std::function<void ()>& function );
			static void Run( const TaskId& taskId );

			static void RunAll();
			static void Wait( const TaskId& taskId );
			static void WaitAll();

		private:		

			static Task* GetScheduledTask();

			static Task* WaitUntilTaskIsAvailable();
			static void HelpWithWork();
			static void WorkOnTask( Task* task );

			static void FinishTask( Task* task );

			static void Scheduler();

			static void ClearTasks();
			
			static inline unsigned int GetTaskOffset( Task* task )
			{
				return task - reinterpret_cast<Task*>( taskPoolMemory );
			}

			static inline TaskId GetTaskId( Task* task )
			{
				return TaskId( GetTaskOffset( task ), task->Generation );
			}
 
			static inline Task* GetTask( TaskId taskId )
			{
				return reinterpret_cast<Task*>( taskPoolMemory ) + taskId.Offset;
			}

			static inline Task* GetTask( unsigned int taskOffset )
			{
				return reinterpret_cast<Task*>( taskPoolMemory ) + taskOffset;
			}

			static inline bool IsTaskFinished( const TaskId& taskId )
			{
				Task* task = GetTask( taskId );
				if( task->Generation != taskId.Generation )
				{
					// task is from an older generation and has been recycled again, so it's been finished already
					return true;
				}
				else
				{
					if( task->OpenTasks == 0 )
						return true;
				}
 
				return false;
			}

			static inline bool CanExecuteTask( Task* task )
			{
				return task->OpenTasks == 1;
			}

			static bool isInitialized;
			static bool isRunning;

			static std::queue<Task*> tasks;

			static std::vector<std::thread> threads;
			static std::condition_variable noJobCond;
			static std::mutex mut;
			
			static const unsigned int TASK_POOL_SIZE = sizeof(Task) * 1024;
			static char taskPoolMemory[TASK_POOL_SIZE];
			static std::unique_ptr<FreeList> mTaskPool;
	};
}

#endif